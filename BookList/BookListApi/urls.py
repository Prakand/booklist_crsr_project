from django.urls import path
from . import views

urlpatterns = [
    # path('books/', views.book),
    path('books/', views.BookList.as_view()),
    path("books/<int:pk>", views.Book.as_view()),
    path('menu-item/', views.MenuItemView.as_view()),
    path('menu-item/<int:pk>', views.SingleMenuItemView.as_view())
]
