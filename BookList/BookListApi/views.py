from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework import generics
from .models import MenuItem
from .serializers import MenuItemSerializer


@api_view()
def book(request):
    return Response("List of the book", status=status.HTTP_200_OK)


class BookList(APIView):
    def get(self, request):
        author = request.GET.get('author')
        if author:
            return Response({"message": "List of book by" + author}, status=status.HTTP_200_OK)
        return Response("List of all the books", status=status.HTTP_200_OK)

    def post(self, request):
        return Response({"message": "BookList has been Created"}, status=status.HTTP_201_CREATED)


class Book(APIView):
    def get(self, request, pk):
        return Response({"message": "Book by id " + str(pk)}, status=status.HTTP_200_OK)

    def put(self, request, pk):
        return Response({"title": request.data.get('title')}, status=status.HTTP_201_CREATED)


class MenuItemView(generics.ListCreateAPIView):
    queryset = MenuItem.objects.all()
    serializer_class = MenuItemSerializer


class SingleMenuItemView(generics.RetrieveUpdateDestroyAPIView):
    queryset = MenuItem.objects.all()
    serializer_class = MenuItemSerializer
